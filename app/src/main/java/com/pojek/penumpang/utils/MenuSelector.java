package com.pojek.penumpang.utils;

/**
 * Created by Androgo on 10/29/2018.
 */

public interface MenuSelector {
    void selectMenu(int position);
}
