package com.pojek.penumpang.home.submenu.home.adapter;

import java.util.List;

// dummy data class
public class DummyPromo{
    private int type;
    private String title;
    private String description;
    private List<DummyChildPromo> dummyChildPromoList;
    private String image;

    public DummyPromo(int type, String title, String description, String image) {
        this.type = type;
        this.title = title;
        this.description = description;
        this.image = image;
    }

    public DummyPromo(int type, String title, String description, List<DummyChildPromo> dummyChildPromoList, String image) {
        this.type = type;
        this.title = title;
        this.description = description;
        this.dummyChildPromoList = dummyChildPromoList;
        this.image = image;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<DummyChildPromo> getDummyChildPromoList() {
        return dummyChildPromoList;
    }

    public void setDummyChildPromoList(List<DummyChildPromo> dummyChildPromoList) {
        this.dummyChildPromoList = dummyChildPromoList;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
