package com.pojek.penumpang.signIn;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.pojek.penumpang.R;
import com.pojek.penumpang.config.Constants;
import com.pojek.penumpang.config.General;
import com.pojek.penumpang.config.LangConfig;
import com.pojek.penumpang.utils.Tools;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LanguageSign extends AppCompatActivity {

    @BindView(R.id.englishRadioButton)
    RadioButton englishRadioButton;
    @BindView(R.id.hindiButton)
    RadioButton hindiButton;
    @BindView(R.id.arabicRadioButton)
    RadioButton arabicRadioButton;
    @BindView(R.id.spanishRadioButton)
    RadioButton spanishRadioButton;
    @BindView(R.id.frenchButton)
    RadioButton frenchButton;
    @BindView(R.id.portugueseButton)
    RadioButton portugueseButton;
    @BindView(R.id.germanButton)
    RadioButton germanButton;
    @BindView(R.id.indonesiaButton)
    RadioButton indonesiaButton;
    @BindView(R.id.img_flag)
    ImageView imgflag;

    private Locale locale;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);
        ButterKnife.bind(this);

        activate_language();
        initToolbar();

        // change language
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        Configuration config = getBaseContext().getResources().getConfiguration();
        String lang = preferences.getString(Constants.LANGUAGE, "");

        if (!"".equals(lang) && !config.locale.getLanguage().equals(lang)) {
            locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }

        switch (lang) {
//            case General.LANGUAGE_EN:
//                englishRadioButton.setChecked(true);
//                hindiButton.setChecked(false);
//                arabicRadioButton.setChecked(false);
//                spanishRadioButton.setChecked(false);
//                frenchButton.setChecked(false);
//                portugueseButton.setChecked(false);
//                germanButton.setChecked(false);
//                indonesiaButton.setChecked(false);
//                englishRadioButton.setTypeface(null, Typeface.BOLD);
//                imgflag.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.lang_uk));
//                break;
//
//            case General.LANGUAGE_HI:
//                englishRadioButton.setChecked(true);
//                hindiButton.setChecked(true);
//                arabicRadioButton.setChecked(false);
//                spanishRadioButton.setChecked(false);
//                frenchButton.setChecked(false);
//                portugueseButton.setChecked(false);
//                germanButton.setChecked(false);
//                indonesiaButton.setChecked(false);
//                hindiButton.setTypeface(null, Typeface.BOLD);
//                imgflag.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.lang_in));
//                break;
//
//            case General.LANGUAGE_AR:
//                englishRadioButton.setChecked(false);
//                hindiButton.setChecked(false);
//                arabicRadioButton.setChecked(true);
//                spanishRadioButton.setChecked(false);
//                frenchButton.setChecked(false);
//                portugueseButton.setChecked(false);
//                germanButton.setChecked(false);
//                indonesiaButton.setChecked(false);
//                arabicRadioButton.setTypeface(null, Typeface.BOLD);
//                imgflag.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.lang_ae));
//                break;
//
//            case General.LANGUAGE_ES:
//                englishRadioButton.setChecked(false);
//                hindiButton.setChecked(false);
//                arabicRadioButton.setChecked(false);
//                spanishRadioButton.setChecked(true);
//                frenchButton.setChecked(false);
//                portugueseButton.setChecked(false);
//                germanButton.setChecked(false);
//                indonesiaButton.setChecked(false);
//                spanishRadioButton.setTypeface(null, Typeface.BOLD);
//                imgflag.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.lang_es));
//                break;
//
//            case General.LANGUAGE_FR:
//                englishRadioButton.setChecked(false);
//                hindiButton.setChecked(false);
//                arabicRadioButton.setChecked(false);
//                spanishRadioButton.setChecked(false);
//                frenchButton.setChecked(true);
//                portugueseButton.setChecked(false);
//                germanButton.setChecked(false);
//                indonesiaButton.setChecked(false);
//                frenchButton.setTypeface(null, Typeface.BOLD);
//                imgflag.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.lang_fr));
//                break;
//
//            case General.LANGUAGE_PT:
//                englishRadioButton.setChecked(false);
//                hindiButton.setChecked(false);
//                arabicRadioButton.setChecked(false);
//                spanishRadioButton.setChecked(false);
//                frenchButton.setChecked(false);
//                portugueseButton.setChecked(true);
//                germanButton.setChecked(false);
//                indonesiaButton.setChecked(false);
//                portugueseButton.setTypeface(null, Typeface.BOLD);
//                imgflag.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.lang_pt));
//                break;
//
//            case General.LANGUAGE_DE:
//                englishRadioButton.setChecked(false);
//                hindiButton.setChecked(false);
//                arabicRadioButton.setChecked(false);
//                spanishRadioButton.setChecked(false);
//                frenchButton.setChecked(false);
//                portugueseButton.setChecked(false);
//                germanButton.setChecked(true);
//                indonesiaButton.setChecked(false);
//                germanButton.setTypeface(null, Typeface.BOLD);
//                imgflag.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.lang_de));
//                break;
//
//            case General.LANGUAGE_IN:
//                englishRadioButton.setChecked(false);
//                hindiButton.setChecked(false);
//                arabicRadioButton.setChecked(false);
//                spanishRadioButton.setChecked(false);
//                frenchButton.setChecked(false);
//                portugueseButton.setChecked(false);
//                germanButton.setChecked(false);
//                indonesiaButton.setChecked(true);
//                indonesiaButton.setTypeface(null, Typeface.BOLD);
//                imgflag.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.lang_id));
//                break;

            default:
                englishRadioButton.setChecked(true);
                hindiButton.setChecked(false);
                arabicRadioButton.setChecked(false);
                spanishRadioButton.setChecked(false);
                frenchButton.setChecked(false);
                portugueseButton.setChecked(false);
                germanButton.setChecked(false);
                indonesiaButton.setChecked(false);
                englishRadioButton.setTypeface(null, Typeface.BOLD);
                imgflag.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.lang_uk));
                break;
        }

//        englishRadioButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                change(General.LANGUAGE_EN);
//                setLangRecreate(General.LANGUAGE_EN);
//
//            }
//        });
//
//        hindiButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                change(General.LANGUAGE_HI);
//                setLangRecreate(General.LANGUAGE_HI);
//
//            }
//        });
//
//        arabicRadioButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                change(General.LANGUAGE_AR);
//                setLangRecreate(General.LANGUAGE_AR);
//            }
//
//        });
//        spanishRadioButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                change(General.LANGUAGE_ES);
//                setLangRecreate(General.LANGUAGE_ES);
//            }
//
//        });
//
//        frenchButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                change(General.LANGUAGE_FR);
//                setLangRecreate(General.LANGUAGE_FR);
//            }
//        });
//
//        portugueseButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                change(General.LANGUAGE_PT);
//                setLangRecreate(General.LANGUAGE_PT);
//            }
//        });
//
//        germanButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                change(General.LANGUAGE_DE);
//                setLangRecreate(General.LANGUAGE_DE);
//            }
//        });
//
//        indonesiaButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                change(General.LANGUAGE_IN);
//                setLangRecreate(General.LANGUAGE_IN);
//            }
//        });

    }

    public void setLangRecreate(String lang) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        locale = new Locale(lang);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        recreate();
        Intent i = new Intent(this, SignInActivity.class);
        startActivity(i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();
    }
    private void change (String lang){
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit()
                .putString(Constants.LANGUAGE, lang)
                .commit();
    }

    private void activate_language(){

        //Active choice of ARABIC
        if (LangConfig.LANGUAGE_EN){
            englishRadioButton.setVisibility(View.VISIBLE);
        }else {
            englishRadioButton.setVisibility(View.GONE);
        }
        //Active choice of ARABIC
        if (LangConfig.LANGUAGE_AR){
            arabicRadioButton.setVisibility(View.VISIBLE);
        }else {
            arabicRadioButton.setVisibility(View.GONE);
        }

        //Active choice of HINDI
        if (LangConfig.LANGUAGE_HI){
            hindiButton.setVisibility(View.VISIBLE);
        }else {
            hindiButton.setVisibility(View.GONE);
        }

        //Active choice of GERMAN
        if (LangConfig.LANGUAGE_DE){
            germanButton.setVisibility(View.VISIBLE);
        }else {
            germanButton.setVisibility(View.GONE);
        }

        //Active choice of Spanish
        if (LangConfig.LANGUAGE_ES){
            spanishRadioButton.setVisibility(View.VISIBLE);
        }else {
            spanishRadioButton.setVisibility(View.GONE);
        }

        //Active choice of French
        if (LangConfig.LANGUAGE_FR){
            frenchButton.setVisibility(View.VISIBLE);
        }else {
            frenchButton.setVisibility(View.GONE);
        }

        //Active choice of Indonesia
        if (LangConfig.LANGUAGE_IN){
            indonesiaButton.setVisibility(View.VISIBLE);
        }else {
            indonesiaButton.setVisibility(View.GONE);
        }

        //Active choice of  Portuguese
        if (LangConfig.LANGUAGE_PT){
            portugueseButton.setVisibility(View.VISIBLE);
        }else {
            portugueseButton.setVisibility(View.GONE);
        }

    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.grey_900);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            default:
                return super.onOptionsItemSelected(menuItem);
        }
        return true;
    }
}
