package com.pojek.penumpang.config;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.pojek.penumpang.R;


public class General {
    //please insert Server_KEY CLOUD_MESSAGING
    public static final String FCM_KEY = "AAAAwOBorlU:APA91bEA4SAGb0Is90GuGj8gLmHeZkweafqQu66S17IAgC1JvaEbw3sRyem13ixPa1szX1tmYJzEHbu19TzoPR98RjsQ4Vx5PlgN530hVcGbJbvGJdtldv2LXlgecUd3zt_w3JvDcpx1";
    public static final LatLngBounds BOUNDS = new LatLngBounds(
            new LatLng(-33.8688197, 0),
            new LatLng(0, 151.20929550000005));

    public static final String ITEM_PURCHASE_CODE =  "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX";
    // Currency settings
    public static final String MONEY = "Rp";

    //number sos
    public static final String NUMBER_SOS = "082221655758";

    //if you use RTL Language e.g : Arabic Language or other, set true
    public static final boolean ENABLE_RTL_MODE = false;

    // if you use distance in KM then
    public static final String UNIT_OF_DISTANCE = "Km"; //if you use km or miles
    public static final Float RANGE_VALUE = 1000f; //if using km (1000f) or Miles using 1609f

    //Setting menu names on Home
    public static final String Name_TIMCar = "Mobil";
    public static final String Name_TIMJek = "Motor";
    public static final String Name_TIMSend = "Pengiriman";
    public static final String Name_TIMFood = "Makanan";
    public static final String Name_Shop = "Belanja";
    public static final String Name_TIMTreatment = "Pijat";
    public static final String Name_TimPayment = "Payment";
    public static final String Name_TimService = "Laundry";

    //new

    public static final boolean ENABLE_SMS_VERIFICATION = true;
    //Setting ADMOB
    public static final boolean ENABLE_ADMOB_BANNER_ADS = true;
    public static final boolean ENABLE_ADMOB_INTERSTITIAL_ADS = true;
    public static final int ADMOB_INTERSTITIAL_ADS_INTERVAL = 3;

    //SLIDE BANNER PROMOTION HOME AND GO-FOOD
    public static final int TIMER_SLIDE = 6000; // default 6 sekon

    // driver order waiting time
    public static final int TIMER_WAITING = 30000; // default 30 sekon
}
