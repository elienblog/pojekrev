package com.pojek.penumpang.home.submenu.home.adapter;

// data class
public class DummyChildPromo{
    private String title;
    private String image;

    public DummyChildPromo(String title, String image) {
        this.title = title;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
