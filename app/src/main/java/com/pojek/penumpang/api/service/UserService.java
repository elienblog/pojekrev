package com.pojek.penumpang.api.service;

import com.pojek.penumpang.model.json.book.RateDriverRequestJson;
import com.pojek.penumpang.model.json.book.RateDriverResponseJson;
import com.pojek.penumpang.model.json.fcm.CancelBookRequestJson;
import com.pojek.penumpang.model.json.fcm.CancelBookResponseJson;
import com.pojek.penumpang.model.json.menu.HelpRequestJson;
import com.pojek.penumpang.model.json.menu.HelpResponseJson;
import com.pojek.penumpang.model.json.menu.HistoryRequestJson;
import com.pojek.penumpang.model.json.menu.HistoryResponseJson;
import com.pojek.penumpang.model.json.menu.VersionRequestJson;
import com.pojek.penumpang.model.json.menu.VersionResponseJson;
import com.pojek.penumpang.model.json.user.ChangePasswordRequestJson;
import com.pojek.penumpang.model.json.user.ChangePasswordResponseJson;
import com.pojek.penumpang.model.json.user.GetBannerResponseJson;
import com.pojek.penumpang.model.json.user.GetFiturResponseJson;
import com.pojek.penumpang.model.json.user.GetSaldoRequestJson;
import com.pojek.penumpang.model.json.user.GetSaldoResponseJson;
import com.pojek.penumpang.model.json.user.LoginRequestJson;
import com.pojek.penumpang.model.json.user.LoginResponseJson;
import com.pojek.penumpang.model.json.user.PulsaRequestJson;
import com.pojek.penumpang.model.json.user.PulsaResponseJson;
import com.pojek.penumpang.model.json.user.RegisterRequestJson;
import com.pojek.penumpang.model.json.user.RegisterResponseJson;
import com.pojek.penumpang.model.json.user.TopupRequestJson;
import com.pojek.penumpang.model.json.user.TopupResponseJson;
import com.pojek.penumpang.model.json.user.UpdateProfileRequestJson;
import com.pojek.penumpang.model.json.user.UpdateProfileResponseJson;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Androgo on 10/13/2018.
 */
    public interface UserService {

        @POST("customer/login")
        Call<LoginResponseJson> login(@Body LoginRequestJson param);

        @POST("customer/register_user")
        Call<RegisterResponseJson> register(@Body RegisterRequestJson param);

        @POST("customer/get_saldo")
        Call<GetSaldoResponseJson> getSaldo(@Body GetSaldoRequestJson param);

        @GET("customer/detail_fitur")
        Call<GetFiturResponseJson> getFitur();

        @POST("customer/user_send_help")
        Call<HelpResponseJson> sendHelp(@Body HelpRequestJson param);

        @POST("customer/update_profile")
        Call<UpdateProfileResponseJson> updateProfile(@Body UpdateProfileRequestJson param);

        @POST("customer/change_password")
        Call<ChangePasswordResponseJson> changePassword(@Body ChangePasswordRequestJson param);

        @POST("book/user_cancel_transaction")
        Call<CancelBookResponseJson> cancelOrder(@Body CancelBookRequestJson param);

        @POST("customer/check_version")
        Call<VersionResponseJson> checkVersion(@Body VersionRequestJson param);

        @POST("book/user_rate_driver")
        Call<RateDriverResponseJson> rateDriver(@Body RateDriverRequestJson param);

        @POST("customer/verifikasi_topup")
        Call<TopupResponseJson> topUp(@Body TopupRequestJson param);

        @POST("customer/complete_transaksi")
        Call<HistoryResponseJson> getCompleteHistory(@Body HistoryRequestJson param);

        @POST("customer/inprogress_transaksi")
        Call<HistoryResponseJson> getOnProgressHistory(@Body HistoryRequestJson param);

        @GET("customer/banner_promosi")
        Call<GetBannerResponseJson> getBanner();
    }