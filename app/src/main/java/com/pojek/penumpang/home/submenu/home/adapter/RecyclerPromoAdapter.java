package com.pojek.penumpang.home.submenu.home.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.pojek.penumpang.R;
import com.pojek.penumpang.home.submenu.setting.AdsActivity;
import com.pojek.penumpang.utils.Log;

import java.util.ArrayList;
import java.util.List;

public class RecyclerPromoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<DummyPromo> items = new ArrayList<>();

    // type of promo
    private static final int STATUS_PROMO_BUTTON_IN = 1;
    private static final int STATUS_PROMO_BUTTON_OUT = 2;
    private static final int STATUS_PROMO_SLIDE = 3;

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder = null;
        Log.d("PROMO_ADAPTER", "viewType: "+String.valueOf(i));
        switch (i){
            case STATUS_PROMO_BUTTON_IN:
                Log.d("PROMO_ADAPTER", "Promo Button In");
                viewHolder =  new ViewHolderButtonIn(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_promo_button_in, viewGroup, false));
                break;
            case STATUS_PROMO_BUTTON_OUT:
                Log.d("PROMO_ADAPTER", "Promo Button Out");
                viewHolder =  new ViewHolderButtonOut(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_promo_button_out, viewGroup, false));
                break;
            case STATUS_PROMO_SLIDE:
                Log.d("PROMO_ADAPTER", "Promo Button Slide");
                viewHolder =  new ViewHolderSlide(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_promo_slide, viewGroup, false));
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ViewHolderButtonIn) {
            Log.d("PROMO_ADAPTER", "View Holder Promo Button In");
            ViewHolderButtonIn viewHolderButtonIn = (ViewHolderButtonIn) viewHolder;
            viewHolderButtonIn.bind(items.get(i));
        }else if (viewHolder instanceof ViewHolderButtonOut) {
            Log.d("PROMO_ADAPTER", "View Holder Promo Button Out");
            ViewHolderButtonOut viewHolderButtonOut = (ViewHolderButtonOut) viewHolder;
            viewHolderButtonOut.bind(items.get(i));
        }else if (viewHolder instanceof ViewHolderSlide) {
            Log.d("PROMO_ADAPTER", "View Holder Promo Button Slide");
            ViewHolderSlide viewHolderSlide = (ViewHolderSlide) viewHolder;
            viewHolderSlide.bind(items.get(i));
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setRecyclerData(List<DummyPromo> data){
        items.clear();
        items.addAll(data);
        notifyDataSetChanged();
    }

    // viewholder for promo button in
    public class ViewHolderButtonIn extends RecyclerView.ViewHolder {
        private ImageView ivPromo;
        private TextView tvTitlePromo;
        private TextView btnPromo;

        public ViewHolderButtonIn(@NonNull View itemView) {
            super(itemView);
            ivPromo = itemView.findViewById(R.id.iv_promo_in);
            tvTitlePromo = itemView.findViewById(R.id.tv_title_promo_in);
            btnPromo = itemView.findViewById(R.id.btn_promo_in);
        }

        public void bind(DummyPromo data){
            Glide.with(itemView.getContext()).load(data.getImage()).centerCrop().into(ivPromo);
            tvTitlePromo.setText(data.getTitle());
            btnPromo.setText("Lihat Promo");
            btnPromo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    String url = "http://m.detik.com";
//                    Intent i = new Intent(Intent.ACTION_VIEW);
//                    i.setData(Uri.parse(url));
//                    itemView.getContext().startActivity(i);

//                    itemView.getContext().startActivity(new Intent(itemView.getContext(), AdsActivity.class));

                    itemView.getContext().startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse(
                                    "https://api.whatsapp.com/send?phone=+6282221655758"
                            )));
                }
            });
        }
    }

    // viewholder for promo button out
    public class ViewHolderButtonOut extends RecyclerView.ViewHolder {
        private ImageView ivPromo;
        private TextView tvTitlePromo;
        private TextView tvDescPromo;
        private TextView btnPromo;

        public ViewHolderButtonOut(@NonNull View itemView) {
            super(itemView);
            ivPromo = itemView.findViewById(R.id.iv_promo_out);
            tvTitlePromo = itemView.findViewById(R.id.tv_title_promo_out);
            tvDescPromo = itemView.findViewById(R.id.tv_desc_promo_out);
            btnPromo = itemView.findViewById(R.id.btn_promo_out);
        }

        public void bind(DummyPromo data){
            Glide.with(itemView.getContext()).load(data.getImage()).centerCrop().into(ivPromo);
            tvTitlePromo.setText(data.getTitle());
            tvDescPromo.setText(data.getDescription());
            btnPromo.setText("Lihat Promo");
            btnPromo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(itemView.getContext(), "Button Clicked!", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    // viewholder for promo slide
    public class ViewHolderSlide extends RecyclerView.ViewHolder {
        private TextView tvTitlePromo;
        private TextView tvDescPromo;
        private RecyclerView recyclerPromoChild;

        private RecyclerPromoChildAdapter mAdapter;

        public ViewHolderSlide(@NonNull View itemView) {
            super(itemView);
            tvTitlePromo = itemView.findViewById(R.id.tv_title_promo_slide);
            tvDescPromo = itemView.findViewById(R.id.tv_desc_promo_slide);
            recyclerPromoChild = itemView.findViewById(R.id.recycler_promo_child);
        }

        public void bind(DummyPromo data){
            tvTitlePromo.setText(data.getTitle());
            tvDescPromo.setText(data.getDescription());
            setupChildAdapter(itemView.getContext());
            mAdapter.setRecyclerData(data.getDummyChildPromoList());
        }

        private void setupChildAdapter(Context context){
            mAdapter = new RecyclerPromoChildAdapter();
            recyclerPromoChild.setAdapter(mAdapter);
            recyclerPromoChild.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            recyclerPromoChild.setItemAnimator(new DefaultItemAnimator());
        }
    }

}
